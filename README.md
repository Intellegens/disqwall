# DisqWall #


### What is DisqWall? ###

DisqWall is a completely customizable easy-to-add widget that you can add to any web page to display most popular comments from the Disqus comment system. Simple to add with just a few lines of HTML code, this solution will help you engage with your audience and get them interacting more and more with your content.

### How to add DisqWall to my site? ###

All you need is to do is add these few lines of code:


    <div ng-app="disqwallApp" class="disqwall"> 
      <disqwall 
          key="YourPublicDisqusAPIKey"
          forum="YourForumName"
          linkto="post"
          type="latest"
          template="app/Disqwall/disqwallWall.html"
          limit="5">
      </disqwall>
    </div>


As you can see for yourself, the DisqWall is completely customizable. Most common parameters are defined within the HTML tag parameters.

For additional (both graphical and functional) customizations, it is possible to add a completely custom HTML template, as well as a completely custom CSS file to make it look and feel like a part of your brand.

### What are the technologies? ###

DisqWall is an open-source project made with AngularJS, HTML and CSS.

This allows it to be cross-browser compatible, easy to implement regardless of the platform or the technology used.

It will require the pages that you want to have the DisqWall on to have some JS files included.