commonApp.service('commonSvc', function() {
    
    return({
    formatTime : formatTime,
    trimStringTo : trimStringTo,
    strip : strip
    });

    function formatTime(dateString) {
        date = Date.parse(dateString);
        var howLong = "";
        var diff = Math.floor(Math.abs(new Date() - date) / 1000 / 60);
        if (diff < 60) {
            howLong = diff.toString() + " minutes ago";
        } else {
            diff = Math.floor(diff / 60);
            if (diff < 24) {
                howLong = diff.toString() + " hours ago";
            } else {
                diff = Math.floor(diff / 24);
                if (diff < 30) {
                    howLong = diff.toString() + " days ago";
                } else {
                    diff = Math.floor(diff / 30);
                    howLong = diff.toString() + " months ago";
                }
            }
        }
        if (diff == 1) {
            howLong = howLong.replace("s ago", " ago");
        }
    
        return howLong;
    }
    
    function trimStringTo(message, length) {
        var newMessage = message;
        if (message.length > length) {
            newMessage = message.substr(0, length);
            if (newMessage.lastIndexOf(' ') != -1) {
                newMessage = newMessage.substr(0, newMessage.lastIndexOf(' '));
            }
            newMessage += '...';
        } 
        return newMessage;
    }
    
    function strip(html) {
         return String(html).replace(/<[^>]+>/gm, '');
    }
    
});