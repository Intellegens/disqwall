disqwallApp.service('disqwallSvc', function($http, $window) {
   
   //Disquss API key and forum name
   
   var APIkey = "";
   var forumName = ":all"; 
   
   // where it links to
   
   var linkTo = 'post'; //post or thread[default]
   
   //Type of list to show
   
   var type = 'latest'; // latest[default] or popular
   
   //Not to be tempered with
   
   var url = 'https://disqus.com/api/3.0/posts/list.json';
   var limit = '25';
   //var order = '';
   //Return public API 
   return({
    getPosts : getPosts,
    formatPosts : formatPosts,
    setParameters: setParameters,
    createLink : createLink,
    initialize : initialize,
    getLinkTo : getLinkTo
    });

    
    // Private methods
    
   function initialize(key, name, link, ty, lim) {
         
         APIkey = key;
         forumName = name; 
         linkTo = link; //post or thread[default]
         type = ty; // latest[default] or popular
         limit = lim;
   
   }
   
   function setParameters() {
      
      
      if (type == 'latest') {
         url = 'https://disqus.com/api/3.0/posts/list.json'
      } else if (type == 'popular') {
         url = 'https://disqus.com/api/3.0/posts/listPopular.json';
      } 
      
    }
    
   function getLinkTo() {
      
      return linkTo;
    }
    
   function createLink(url, id, link) {
      if (link == 'post') {
         return url+'#comment-'+id;
      } else {
         return url;
      }
    }
    

    
    //Public methods

    function getPosts() {
      
        var promise = $http({
                        url: url, 
                        method: "GET",
                        params: {api_key : APIkey,
                                 forum: forumName,
                                 related: 'thread',
                                 limit : limit
                                 }
                     }).then(function(resp) {
                        
                        return resp.data;
                        
                     });

        
        return promise;
    }
    
    
    
    function formatPosts(data) {
        return data.response;
    }
    
});