disqwallApp.directive('disqwall', function() {
  return {
    restrict: 'E',
    scope: {
            key: "@key",
            forum: "@forum",
            linkto: "@linkto",
            type: '@type',
            template: '@template',
            limit: '@limit'
        },
        template:'<div ng-include="getContentUrl()"></div>',
    
        controller: function($scope, $window, disqwallSvc, commonSvc) {
            
            //Disquss API key and forum name
           
            // latest[default] or popular
           
           
            disqwallSvc.initialize($scope.key, $scope.forum, $scope.linkto, $scope.type, $scope.limit); // init svc variables
            disqwallSvc.setParameters();
            disqwallSvc.getPosts().then(function(d) {
                $scope.data = disqwallSvc.formatPosts(d);

            });
            
            $scope.createLink = disqwallSvc.createLink;
            $scope.getLinkTo = disqwallSvc.getLinkTo;
            
            $scope.formatTime = commonSvc.formatTime;
            $scope.trimStringTo = commonSvc.trimStringTo;
            $scope.strip = commonSvc.strip;
            
        },
        
        link: function ($scope, $element, $attrs) {
            $scope.key = $attrs.key;
            $scope.forum = $attrs.forum;
            $scope.linkto = $attrs.linkto;
            $scope.type = $attrs.type;
            $scope.limit = $attrs.limit;
            $scope.getContentUrl = function() {
                return $attrs.template;
            }
        }
        
  };
})



